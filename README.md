# MQTT YC600 Homie

Exposes all measurements of the double solar power inverter from APsystems YC600 to an MQTT Broker with the Homie standardization.
To work properly an TI CC2530/2531 board with a custom firmware has to be used. See "CC2530/2531-firmware" folder for this.
The software is written with easy access in mind. So the user only has to define which pins he use to communicate with the CC2530/2531. These should be real hardware serial pins, either 3,1 or 13,15 or (untested) by software serial the pin you would like to have.

## license
- https://creativecommons.org/licenses/by-nc-sa/3.0/ CC BY-NC-SA 3.0
- Use at your own risk
- everything is tested as good as I can but I couldn't give any guarantee
## Credits goes to:
- Discord Channel: https://discord.com/channels/801799706744717322/801799706744717325
- Github Discussion: https://github.com/Koenkk/zigbee2mqtt/issues/4221
- The Discord users: @kadzsol#5835 and @mujuli[DE]#5398 which developed the CC2530/2531 firmware
- The Discord user: @Skyscraper#8576 who helped with data extraction calculations
- I got some inspiration by: https://github.com/ingeniuske/CSE7766

## Feature list:
- Auto pairing
- features a maximum of 5 solar power inverters
- configurable with the homie setup android app

## Dependencies
- depends on https://gitlab.com/RunningPenguin/YC600_serial Version 0.0.8
- depends on https://github.com/marvinroger/homie-esp8266.git Version 3.0.1

## flashing of CC2530
To flash the CC2530 I used this tutorial/method: https://zigbee.blakadder.com/flashing_ccloader.html
## Download

This Git repository contains the development version of the Homie implementation which uses the library from https://gitlab.com/RunningPenguin/YC600_serial. It is possible that there are some bugs. But these version is tested for a little bit more than 1 year

## Installation
1. Just compile and upload to an Wemos D1 Mini.
2. Connect the D1 Mini to the CC2530/2531
3. Connect your phone to the Access Point from you Wemos D1 Mini (the one with the name "Homie-***")
4. Start the "Homie setup" App
5. fill in your WiFi Credentials and MQTT settings
6. fill in your serial numbers of your AP YC600 solar power inverters
![](./pictures/SetupSerialNumber.png)
7. wait till the sun is shining (the YC600 transmit section is powered by solar)
8. after like 10-15 minutes the defice should publish data to your MQTT broker

## Project status
Tested for 1 year in productive state to read out two AP Systems YC600 solar inverters.

