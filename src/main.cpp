#include <Homie.h>

#define FW_NAME "Homie-YC600"
#define FW_VERSION "0.0.1"

/* Magic sequence for Autodetectable Binary Upload */
const char *__FLAGGED_FW_NAME = "\xbf\x84\xe4\x13\x54" FW_NAME "\x93\x44\x6b\xa7\x75";
const char *__FLAGGED_FW_VERSION = "\x6a\x3f\x3e\x0e\xe1" FW_VERSION "\xb0\x30\x48\xd4\x1a";
/* End of magic sequence for Autodetectable Binary Upload */

/* -------------------------OTA update -------------------*/
// #include <ArduinoOTA.h> not possible too less flash space
/* -------------------------------------------------------*/

#include <Arduino.h>

#define LED_PIN_BLUE D4

// has to be before the include of YC600_serial.h so it overrides the default of "5" in YC600_serial.h
#define YC600_MAX_NUMBER_OF_INVERTERS 5

// if you encounter issues which could be corrected by multiple resets please increase this value #define has to be before YC600_serial.h
#define CC2530_STARTUP_RESET_TIMES 2

#define YC600_HOMIE_RX_PIN 13
#define YC600_HOMIE_TX_PIN 15

#include "YC600Node.hpp"

YC600Node yc600node("YC600", "YC600");

// #define SOFTWARE_SERIAL_DEBUG //if you define this you have to undefine the debugging of the inner lib

#ifdef SOFTWARE_SERIAL_DEBUG
// the library uses HW serial in this example so we use SW serial for the output of the inverters to our PC
#include <SoftwareSerial.h>
SoftwareSerial DebugSwSer(14, 12); //Define hardware connections serial console for debuggin reasons to read the actual values
#endif

bool _mqtt_ready = false;
bool _temp_HomieInitComplete = false;
void onHomieEvent(const HomieEvent &event)
{
    switch (event.type)
    {
        //    case HomieEventType::STANDALONE_MODE:
        //      // Do whatever you want when standalone mode is started
        //      break;
        //    case HomieEventType::CONFIGURATION_MODE:
        //      // Do whatever you want when configuration mode is started
        //      break;
        //    case HomieEventType::NORMAL_MODE:
        //      // Do whatever you want when normal mode is started
        //      break;
        //    case HomieEventType::OTA_STARTED:
        //      // Do whatever you want when OTA is started
        //      break;
        //    case HomieEventType::OTA_PROGRESS:
        //      // Do whatever you want when OTA is in progress
        //      // You can use event.sizeDone and event.sizeTotal
        //      break;
        //    case HomieEventType::OTA_FAILED:
        //      // Do whatever you want when OTA is failed
        //      break;
        //    case HomieEventType::OTA_SUCCESSFUL:
        //      // Do whatever you want when OTA is successful
        //      break;
        //    case HomieEventType::ABOUT_TO_RESET:
        //      // Do whatever you want when the device is about to reset
        //      break;
        //    case HomieEventType::WIFI_CONNECTED:
        //      // Do whatever you want when Wi-Fi is connected in normal mode
        //      // You can use event.ip, event.gateway, event.mask
        //      break;
        //    case HomieEventType::WIFI_DISCONNECTED:
        //      // Do whatever you want when Wi-Fi is disconnected in normal mode
        //      // You can use event.wifiReason
        //      break;
    case HomieEventType::MQTT_READY:
        _mqtt_ready = true;
        //      // Do whatever you want when MQTT is connected in normal mode
        break;
        //    case HomieEventType::MQTT_DISCONNECTED:
        //      // Do whatever you want when MQTT is disconnected in normal mode
        //      // You can use event.mqttReason
        //      break;
        //    case HomieEventType::MQTT_PACKET_ACKNOWLEDGED:
        //      // Do whatever you want when an MQTT packet with QoS > 0 is acknowledged by the broker
        //      // You can use event.packetId
        //      break;
        //    case HomieEventType::READY_TO_SLEEP:
        //      // After you've called `prepareToSleep()`, the event is triggered when MQTT is disconnected
        //      break;
    }
}

void loopHandler()
{
    if (_temp_HomieInitComplete == true && _mqtt_ready == true)
    {
        yc600node.tick();
    }
}

void setupHandler()
{
    _temp_HomieInitComplete = yc600node.Initialize(YC600_HOMIE_RX_PIN, YC600_HOMIE_TX_PIN);
}

void setup()
{
#ifdef SOFTWARE_SERIAL_DEBUG
    DebugSwSer.begin(115200); //Initialize software serial with baudrate of 115200
    while (!DebugSwSer)
        ;
    DebugSwSer.println();
#endif

#ifndef SOFTWARE_SERIAL_DEBUG
    Homie.disableLogging(); // before Homie.setup()
#endif

    Homie_setFirmware(FW_NAME, FW_VERSION);

    Homie.setLedPin(LED_PIN_BLUE, LOW);

#ifdef SOFTWARE_SERIAL_DEBUG
    Homie.setLoggingPrinter(&DebugSwSer);
#endif
    Homie.onEvent(onHomieEvent);
    Homie.setSetupFunction(setupHandler);
    Homie.setLoopFunction(loopHandler);

    yc600node.beforeSetup();

    Homie.getLogger() << "main.c beforeSetup " << endl;

    Homie.setup();
}

void loop()
{
    Homie.loop();
}