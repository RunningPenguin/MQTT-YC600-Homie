#ifndef _YC_600Node_hpp_
#define _YC_600Node_hpp_
/*
 * YC600Node.hpp
 * Homie node for YC600 Inverter from APSystems
 *
 * Version: 0.0.2
 * Author: Tobias Sachs (https://gitlab.com/RunningPenguin)
 */

#include <Homie.hpp>

// has to be before the include of YC600_serial.h so it overrides the default of "5" in YC600_serial.h
#define YC600_MAX_NUMBER_OF_INVERTERS 5

// if you encounter issues which could be corrected by multiple resets please increase this value #define has to be before YC600_serial.h
#define CC2530_STARTUP_RESET_TIMES 2

#include "YC600_serial.h"

#ifndef YC600_HOMIE_RX_PIN
#define YC600_HOMIE_RX_PIN 13
#endif

#ifndef YC600_HOMIE_TX_PIN
#define YC600_HOMIE_TX_PIN 15
#endif

class YC600Node : public HomieNode
{
private:
  bool _tempStructReady = false;
  bool _tempResetDone = false;
  bool _tempYC600InitDone = false;
  bool _first_message_decoded = false;
  bool _triedToPair = false;

  int _homie_pin_rx = YC600_HOMIE_RX_PIN;
  int _homie_pin_tx = YC600_HOMIE_TX_PIN;

  unsigned long _lastYC600Sent = 0;

  char _homie_inv_sns[YC600_MAX_NUMBER_OF_INVERTERS][20] = {{0}};

  uint8_t _homie_number_of_inverters = 0;
  uint8_t _numberOfSavedInverters = 0;
  uint8_t _numberOfConfiguredInverters = 0;

  bool _advertise(uint8_t number_of_inverters_to_advertise);
  // Settable properties
  const char *cPropYC600State = "stateecu";
  const char *cPropFrequency = "frequencyinv";
  const char *cPropTemperature = "temperatureinv";
  const char *cPropAcVoltage = "acvoltageinv";
  const char *cPropCurrentPanel1 = "currentfirstpanelinv";
  const char *cPropCurrentPanel2 = "currentsecondpanelinv";
  const char *cPropVoltagePanel1 = "voltagefirstpanelinv";
  const char *cPropVoltagePanel2 = "voltagesecondpanelinv";
  const char *cPropPowerPanel1 = "powerfirstpanelinv";
  const char *cPropPowerPanel2 = "powersecondpanelinv";
  const char *cPropEnergyPanel1 = "energyfirstpanelinv";
  const char *cPropEnergyPanel2 = "energysecondpanelinv";
  const char *cPropInverterCount = "invertercount";
  

protected:
  bool _turnProvidedSettingsIntoStruct();
  //  virtual bool handleInput(const HomieRange &range, const String &property, const String &value);
  //  virtual void loop() override;
  void _reboot();

public:
  YC600Node(const char *id, const char *name);

  void tick();

  void beforeSetup();
  bool Initialize(unsigned char pin_rx, unsigned char pin_tx);
  void returnStatusToOpenhab();
  void returnValuesToOpenhab(uint8_t inverterReturnIndex);
};

#endif //_YC_600Node_hpp_