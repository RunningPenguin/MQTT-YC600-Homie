/*
 * YC600Node.cpp
 * Homie node for YC600 Inverter from APSystems
 *
 * Version: 0.0.2
 * Author: Tobias Sachs (https://gitlab.com/RunningPenguin)
 */

#include <YC600Node.hpp>

YC600 myYC600;

// The fake ID which our ESP8266 ECU should use for the binding and communication with the inverters
char _ecu_id[] = "D8A3011B9780"; //in my working ECU it's another ID

const int DEFAULT_YC600_INTERVAL = 5*(YC600_MAX_NUMBER_OF_INVERTERS+1); //we need 5s for each inverter to poll the information and give an extra timeslot of 5s 

HomieSetting<long> YC600IntervalSetting("YC600Interval", "The update interval from the YC600 fake ECU in seconds minimum 5*(Invertercount+1)");

HomieSetting<const char *> settingSerialNumberInverter1("Serial_1", "First YC600 serial number e.g. 4080001181**");
HomieSetting<const char *> settingSerialNumberInverter2("Serial_2", "Second YC600 serial number e.g. 4080001193**");
HomieSetting<const char *> settingSerialNumberInverter3("Serial_3", "Third YC600 serial number e.g. 4080001181**");
HomieSetting<const char *> settingSerialNumberInverter4("Serial_4", "Fourth YC600 serial number e.g. 4080001181**");
HomieSetting<const char *> settingSerialNumberInverter5("Serial_5", "Fifth YC600 serial number e.g. 4080001181**");

YC600Node::YC600Node(const char *id, const char *name)
    : HomieNode(id, name, "YC600 Output")
{
}

// bool YC600Node::handleInput(const HomieRange &range, const String &property, const String &value)
// {
// }

// == -1 we didn't found an inverter short ID by auto pair routine
// == 10 reset of CC2530 detected
// == 12 all inverters are paired at startup
// == 13 not all inverters are paired at startup - need to pair
// == 18 we received a new string from the CC2530 - one or more unknown strings
// == 20 we try to decode a new message
// == 22 we successfully decoded a new message
// == 30 we try to pair the actual inverter
// == 34 we found an inverter short ID by auto pair routine
// == 38 auto pairing is saving actual informations to EEPROM
// == 40 auto pairing successfull saved actual informations to EEPROM
// == 42 after auto pairing paired all inverters
// possible values of myYC600.getState()

// == -1 if no data was extracted since startup yet
// == 0 for the first inverter from the inv_sns struct you gave to the begin function
// == 1 for the second inverter from the inv_sns struct you gave to the begin function
// == 2 ... and so on
// should change every 5s (or if changed every YC600_SYNC_INTERVAL)
// so if you have one inverter you should get new values from this one every 5s
// if you have 2 inverters you get from each individual inverter updates every 10s
// if you have 3 inverters each inverter updates at 15s
// this has the reason that the polling from the Wemos D1 mini pro to the CC2530
// and then over the air (ZigBee) to the YC600 and back takes some time
// and we could only poll inverter at a time
// possible values and update rate of myYC600.getUpdatedInverterNumber()

int8_t actualState = 0;
int8_t oldState = 0;

int8_t actualInverterNumber = -1;
//TODO not needed anymore? int8_t oldInverterNumber = -1;

int8_t readableinverterReturnIndex = -1;

void YC600Node::_reboot()
{
    wdt_disable();
    wdt_enable(WDTO_15MS);
    while (1)
    {
    }
}

char tempReceiveIdBuffer[254] = {0};
char tempIndexStr[10] = {0};
void YC600Node::tick()
{

    myYC600.tick(); //tick function should be called in every loop cycle

    actualState = myYC600.getState();
    if (actualState != oldState)
    {
        oldState = actualState;
        Homie.getLogger() << "**** actualState = " << endl;
        switch (actualState)
        {
        case -1:
        {
            Homie.getLogger() << "not ready" << endl;
            setProperty(cPropYC600State).send("not-ready"); //give Feedback to Openhab
            break;
        }
        case 10:
        {
            Homie.getLogger() << "reset of CC2530" << endl;
            setProperty(cPropYC600State).send("reset-CC2530"); //give Feedback to Openhab
            break;
        }
        case 12:
        {
            Homie.getLogger() << "all inverters are paired at startup" << endl;
            setProperty(cPropYC600State).send("all-paired"); //give Feedback to Openhab
            setProperty(cPropInverterCount).send(String(_numberOfSavedInverters)); //give Feedback to Openhab
            break;
        }
        case 13:
        {
            Homie.getLogger() << "not all inverters are paired at startup - need to pair" << endl;
            setProperty(cPropYC600State).send("need-pairing"); //give Feedback to Openhab
            break;
        }
        case 18:
        {
            Homie.getLogger() << "we received a new string from the CC2530 - one or more unknown strings" << endl;
            setProperty(cPropYC600State).send("new-string"); //give Feedback to Openhab
            //TODO not needed anymore oldInverterNumber = -1;                          //we decoded a new message so we have new values, and if we have more than one inverters paired but only one inverter gives answers we would like to read all answers from this inverter
            break;
        }
            // not working      case 20:
            // not working      {
            // not working        Homie.getLogger() << "we try to decode a new message" << endl;
            // not working        break;
            // not working      }
        case 22:
        {
            Homie.getLogger() << "we successfully decoded a new message" << endl;
            setProperty(cPropYC600State).send("decoded-message"); //give Feedback to Openhab
            _first_message_decoded = true;
            break;
        }
        case 30:
        {
            Homie.getLogger() << "auto pairing - we try to pair one inverter" << endl;
            setProperty(cPropYC600State).send("try-pairing"); //give Feedback to Openhab
            _triedToPair = true;
            break;
        }
            // not working      case 34:
            // not working      {
            // not working        Homie.getLogger() << "we found an inverter short ID by auto pair routine" << endl;
            // not working        break;
            // not working      }
            // not working      case 38:
            // not working      {
            // not working        Homie.getLogger() << "auto pairing is saving actual informations to EEPROM" << endl;
            // not working        break;
            // not working      }
        case 40:
        {
            Homie.getLogger() << "auto pairing - successfull saved inverter serial number, inverter short ID and ECU-ID to EEPROM" << endl;
            setProperty(cPropYC600State).send("inverter-paired"); //give Feedback to Openhab
            break;
        }
        case 42:
        {
            Homie.getLogger() << "auto pairing - paired all inverters successfull" << endl;
            setProperty(cPropYC600State).send("pairing-successfull"); //give Feedback to Openhab
            delayMicroseconds(100);                          //to give homie some time to set the property before we reboot

            if (_triedToPair == true)
            {
                _reboot(); //try to reboot to initialize everything again and start with a clean RAM
            }
            break;
        }
        default:
            break;
        }
    }

    if (millis() - _lastYC600Sent >= YC600IntervalSetting.get() * 1000UL)
    {
        _lastYC600Sent = millis();

        actualInverterNumber = myYC600.getUpdatedInverterNumber();

        if ((actualInverterNumber != -1) && (_first_message_decoded == true))
        {
            for (uint8_t i = 0; i < _numberOfSavedInverters; i++)
            {
                Homie.getLogger() << "_numberOfSavedInverters: " << _numberOfSavedInverters << endl;
                Homie.getLogger() << "returnValuesToOpenhab(i): " << i << endl;
                returnValuesToOpenhab(i);
            }
        }
    }
}

void YC600Node::beforeSetup()
{
    YC600IntervalSetting.setDefaultValue(DEFAULT_YC600_INTERVAL).setValidator([](long candidate)
                                                                              {
                                                                                  if (candidate < DEFAULT_YC600_INTERVAL) //TODO not working like expected
                                                                                  {
                                                                                      candidate = DEFAULT_YC600_INTERVAL;
                                                                                  }
                                                                                  return candidate > 0;
                                                                              });

    settingSerialNumberInverter1.setDefaultValue("").setValidator([](const char *candidate)
                                                                  {
                                                                      if (strlen(candidate) < 12 || strlen(candidate) > 14) //TODO not working like expected
                                                                      {
                                                                          return "";
                                                                      }
                                                                      else
                                                                      {
                                                                          return candidate;
                                                                      }
                                                                  });

    settingSerialNumberInverter2.setDefaultValue("").setValidator([](const char *candidate)
                                                                  {
                                                                      if (strlen(candidate) < 12 || strlen(candidate) > 14) //TODO not working like expected
                                                                      {
                                                                          return "";
                                                                      }
                                                                      else
                                                                      {
                                                                          return candidate;
                                                                      }
                                                                  });

    settingSerialNumberInverter3.setDefaultValue("").setValidator([](const char *candidate)
                                                                  {
                                                                      if (strlen(candidate) < 12 || strlen(candidate) > 14) //TODO not working like expected
                                                                      {
                                                                          return "";
                                                                      }
                                                                      else
                                                                      {
                                                                          return candidate;
                                                                      }
                                                                  });

    settingSerialNumberInverter4.setDefaultValue("").setValidator([](const char *candidate)
                                                                  {
                                                                      if (strlen(candidate) < 12 || strlen(candidate) > 14) //TODO not working like expected
                                                                      {
                                                                          return "";
                                                                      }
                                                                      else
                                                                      {
                                                                          return candidate;
                                                                      }
                                                                  });

    settingSerialNumberInverter5.setDefaultValue("").setValidator([](const char *candidate)
                                                                  {
                                                                      if (strlen(candidate) < 12 || strlen(candidate) > 14) //TODO not working like expected
                                                                      {
                                                                          return "";
                                                                      }
                                                                      else
                                                                      {
                                                                          return candidate;
                                                                      }
                                                                  });

    Homie.getLogger() << "beforeSetup before getNumberOfSavedInverters: " << endl;

    Homie.getLogger() << "Homie.getConfiguration().wifi.ssid: " << Homie.getConfiguration().wifi.ssid << endl;

    // we couldn't call very first begin with an unconfigured device as it results in restarts while configuring with Android "Homie setup" App
    //if (strcmp(Homie.getConfiguration().ConfigStruct.name, "") != 0) //didn't work out as expected, didn't change the behaviour after successfull configuration
    if (Homie.isConfigured())
    {
        Homie.getLogger() << "wifi ssid configured: "
                          << "so I assume the device is configured and it is save to call 'getNumberOfSavedInverters'" << endl;

        Homie.getLogger() << "beforeSetup before getNumberOfSavedInverters: " << endl;
        _numberOfSavedInverters = myYC600.getNumberOfSavedInverters(YC600_MAX_NUMBER_OF_INVERTERS);
        // advertise Homie
        Homie.getLogger() << "beforeSetup _numberOfSavedInverters: " << _numberOfSavedInverters << endl;
        if (_numberOfSavedInverters != 0)
        {
            _advertise(_numberOfSavedInverters);
        }
    }
}

char tempIdBuffer[254] = {0};
char tempAdvertiseIndexStr[10] = {0};
uint8_t readableInvId = 0;
bool YC600Node::_advertise(uint8_t number_of_inverters_to_advertise)
{
    Homie.getLogger() << "_advertise number_of_inverters_to_advertise: " << number_of_inverters_to_advertise << endl;

    advertise(cPropYC600State).setDatatype("enum").setFormat("not-ready,reset-CC2530,all-paired,need-pairing,new-string,decoded-message,try-pairing,inverter-paired,pairing-successfull").settable();
    advertise(cPropInverterCount).setDatatype("integer").setFormat("0:5").setUnit("").settable(); //TODO 5 is hardcoded and needs to be adjusted to YC600_MAX_NUMBER_OF_INVERTERS
    //cPropFrequency
    for (uint8_t j = 0; j < number_of_inverters_to_advertise; j++)
    {
        readableInvId = j + 1;                                               //because humans count most of the time from 1 onwards not from 0
        memset(&tempAdvertiseIndexStr[0], 0, sizeof(tempAdvertiseIndexStr)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                                              //give memset a little bit of time to empty all the buffers
        itoa(readableInvId, tempAdvertiseIndexStr, 10);
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

        //Frequency
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropFrequency, strlen(cPropFrequency));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 1: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Frequency").setDatatype("float").setFormat("%.2f").setUnit("Hz").settable();

        //Temperature
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropTemperature, strlen(cPropTemperature));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 2: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("temperature").setDatatype("float").setFormat("%.2f").setUnit("°C").settable();

        //AC Voltage
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropAcVoltage, strlen(cPropAcVoltage));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 3: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Ac Voltage").setDatatype("float").setFormat("%.2f").setUnit("V").settable();

        //DC Current Panel1
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropCurrentPanel1, strlen(cPropCurrentPanel1));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 4: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Current Panel1").setDatatype("float").setFormat("%.2f").setUnit("A").settable();

        //DC Current Panel2
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropCurrentPanel2, strlen(cPropCurrentPanel2));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 5: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Current Panel2").setDatatype("float").setFormat("%.2f").setUnit("A").settable();

        //DC Voltage Panel1
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropVoltagePanel1, strlen(cPropVoltagePanel1));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 6: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Voltage Panel1").setDatatype("float").setFormat("%.2f").setUnit("V").settable();

        //DC Voltage Panel2
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropVoltagePanel2, strlen(cPropVoltagePanel2));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 7: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Voltage Panel2").setDatatype("float").setFormat("%.2f").setUnit("V").settable();

        //DC Power Panel1
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropPowerPanel1, strlen(cPropPowerPanel1));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 8: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Power Panel1").setDatatype("float").setFormat("%.2f").setUnit("W").settable();

        //DC Power Panel2
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropPowerPanel2, strlen(cPropPowerPanel2));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 9: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Power Panel2").setDatatype("float").setFormat("%.2f").setUnit("W").settable();

        //DC Energy Panel1
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropEnergyPanel1, strlen(cPropEnergyPanel1));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 10: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Energy Panel1").setDatatype("float").setFormat("%.2f").setUnit("kWh").settable();

        //DC Energy Panel2
        memset(&tempIdBuffer[0], 0, sizeof(tempIdBuffer)); //zero out all buffers we could work with "messageToDecode"
        delayMicroseconds(250);                            //give memset a little bit of time to empty all the buffers
        strncpy(tempIdBuffer, cPropEnergyPanel2, strlen(cPropEnergyPanel2));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        strncat(tempIdBuffer, tempAdvertiseIndexStr, sizeof(tempAdvertiseIndexStr));
        delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
        Homie.getLogger() << "_advertise tempIdBuffer 11: " << tempIdBuffer << endl;
        advertise(tempIdBuffer).setName("Energy Panel2").setDatatype("float").setFormat("%.2f").setUnit("kWh").settable();
    }
    return true;
}

bool _tempStructReady = false;
bool _tempResetDone = false;
bool YC600Node::Initialize(unsigned char pin_rx, unsigned char pin_tx)
{
    _homie_pin_rx = pin_rx;
    _homie_pin_tx = pin_tx;

    Homie.getLogger() << "InitializeSerialNumbers: " << endl;

    _tempStructReady = _turnProvidedSettingsIntoStruct();
    Homie.getLogger() << "Initialize _tempStructReady: " << _tempStructReady << endl;

    if (_tempStructReady == true)
    {
        Homie.getLogger() << "one or more correct serial numbers detected!" << endl;
        //serial numbers were okay

        Homie.getLogger() << "**** Initialize actualState = " << myYC600.getState() << endl;
        return true;
    }
    else if (_tempResetDone == false)
    {
        Homie.getLogger() << "only incorrect serial numbers detected!" << endl
                          << "resetting Homie to let you configure it another time" << endl;

        //we dont have the right format in the Inv Serial numbers
        //reset homie into configuration mode
        Homie.reset();
        _tempResetDone = true;
        return false;
    }
    else
    {
        return false;
    }
}

bool homie_sns_struct_ready = false;

bool YC600Node::_turnProvidedSettingsIntoStruct()
{
    if (homie_sns_struct_ready == false && _homie_number_of_inverters == 0)
    {
        if (settingSerialNumberInverter1.wasProvided() && strlen(settingSerialNumberInverter1.get()) == 12) //TODO second comparison only because validator isn't working
        {
            Homie.getLogger() << "settingSerialNumberInverter1: " << settingSerialNumberInverter1.get() << endl;
            Homie.getLogger() << "strlen(settingSerialNumberInverter1.get(): " << strlen(settingSerialNumberInverter1.get()) << endl;
            strncpy(_homie_inv_sns[_homie_number_of_inverters], settingSerialNumberInverter1.get(), strlen(settingSerialNumberInverter1.get()));
            _homie_number_of_inverters++;
        }
        else
        {
            Homie.getLogger() << "settingSerialNumberInverter1 incorrect!" << endl;
        }

        if (settingSerialNumberInverter2.wasProvided() && strlen(settingSerialNumberInverter2.get()) == 12) //TODO second comparison only because validator isn't working
        {
            Homie.getLogger() << "settingSerialNumberInverter2: " << settingSerialNumberInverter2.get() << endl;
            strncpy(_homie_inv_sns[_homie_number_of_inverters], settingSerialNumberInverter2.get(), strlen(settingSerialNumberInverter2.get()));
            _homie_number_of_inverters++;
        }
        else
        {
            Homie.getLogger() << "settingSerialNumberInverter2 incorrect!" << endl;
        }

        if (settingSerialNumberInverter3.wasProvided() && strlen(settingSerialNumberInverter3.get()) == 12) //TODO second comparison only because validator isn't working
        {
            Homie.getLogger() << "settingSerialNumberInverter3: " << settingSerialNumberInverter3.get() << endl;
            strncpy(_homie_inv_sns[_homie_number_of_inverters], settingSerialNumberInverter3.get(), strlen(settingSerialNumberInverter3.get()));
            _homie_number_of_inverters++;
        }
        else
        {
            Homie.getLogger() << "settingSerialNumberInverter3 incorrect!" << endl;
        }

        if (settingSerialNumberInverter4.wasProvided() && strlen(settingSerialNumberInverter4.get()) == 12) //TODO second comparison only because validator isn't working
        {
            Homie.getLogger() << "settingSerialNumberInverter4: " << settingSerialNumberInverter4.get() << endl;
            strncpy(_homie_inv_sns[_homie_number_of_inverters], settingSerialNumberInverter4.get(), strlen(settingSerialNumberInverter4.get()));
            _homie_number_of_inverters++;
        }
        else
        {
            Homie.getLogger() << "settingSerialNumberInverter4 incorrect!" << endl;
        }

        if (settingSerialNumberInverter5.wasProvided() && strlen(settingSerialNumberInverter5.get()) == 12) //TODO second comparison only because validator isn't working
        {
            Homie.getLogger() << "settingSerialNumberInverter5: " << settingSerialNumberInverter5.get() << endl;
            strncpy(_homie_inv_sns[_homie_number_of_inverters], settingSerialNumberInverter5.get(), strlen(settingSerialNumberInverter5.get()));
            _homie_number_of_inverters++;
        }
        else
        {
            Homie.getLogger() << "settingSerialNumberInverter5 incorrect!" << endl;
        }
    }

    Homie.getLogger() << "_homie_number_of_inverters: " << _homie_number_of_inverters << endl;

    if (_homie_number_of_inverters != 0)
    {
        for (uint8_t i = 0; i < YC600_MAX_NUMBER_OF_INVERTERS; i++)
        {
            Homie.getLogger() << "i: " << i << "_homie_inv_sns[i]: " << _homie_inv_sns[i] << endl;
            //it could be possible to strncpy here but only if I could manage to get an array of settingSerialNumberInverter
        }
        homie_sns_struct_ready = true;
    }

    Homie.getLogger() << "homie_sns_struct_ready: " << homie_sns_struct_ready << endl;

    if (homie_sns_struct_ready == true)
    {
        // Initialize YC600
        myYC600.setRxTx(_homie_pin_rx, _homie_pin_tx); //3==HW-RX 1==HW-TX to USB debugging //13==HW-RX alt 15==HW-TX alt to CC2530 // we have to use "Serial.begin" even before the YC600 libs tries to begin

        Homie.getLogger() << "after setRxTx! " << endl;

        myYC600.begin(_ecu_id, _homie_inv_sns); // will initialize HW-serial to 115200 bps

        Homie.getLogger() << "after YC600.begin! " << endl;

        // why?    _numberOfConfiguredInverters = myYC600.calculateNumberOfInverters(*_homie_inv_sns);
        // why?    Homie.getLogger() << "_numberOfConfiguredInverters: " << _numberOfConfiguredInverters << endl;
    }
    else
    {
        Homie.getLogger() << "could not initialize " << endl;
    }

    Homie.getLogger() << "homie_sns_struct_ready: " << homie_sns_struct_ready << endl;

    return homie_sns_struct_ready;
}

void YC600Node::returnStatusToOpenhab()
{
}

void YC600Node::returnValuesToOpenhab(uint8_t inverterReturnIndex)
{

    Homie.getLogger() << "**** InverterNumber = " << inverterReturnIndex << endl;

    Homie.getLogger() << "Decode: frequency: " << myYC600.getAcFrequency(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: temperature: " << myYC600.getInverterTemperature(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: AC voltage: " << myYC600.getAcVoltage(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: current Panel1: " << myYC600.getDcCurrentPanel1(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: current Panel2: " << myYC600.getDcCurrentPanel2(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: voltage Panel1: " << myYC600.getDcVoltagePanel1(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: voltage Panel2: " << myYC600.getDcVoltagePanel2(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: power Panel1: " << myYC600.getDcPowerPanel1(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: power Panel2: " << myYC600.getDcPowerPanel2(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: energy Panel1: " << myYC600.getDcEnergyPanel1(inverterReturnIndex) << endl;

    Homie.getLogger() << "Decode: energy Panel2: " << myYC600.getDcEnergyPanel2(inverterReturnIndex) << endl;

    // tempReceiveIdBuffer
    readableinverterReturnIndex = inverterReturnIndex + 1; //because humans count most of the time from 1 onwards not from 0
    memset(&tempIndexStr[0], 0, sizeof(tempIndexStr));     //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                //give memset a little bit of time to empty all the buffers
    itoa(readableinverterReturnIndex, tempIndexStr, 10);
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers

    //Frequency
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropFrequency, strlen(cPropFrequency));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getAcFrequency(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                     //to give homie some time to set the property before we go on

    //Temperature
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropTemperature, strlen(cPropTemperature));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getInverterTemperature(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                             //to give homie some time to set the property before we go on

    //AC Voltage
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropAcVoltage, strlen(cPropAcVoltage));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getAcVoltage(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                   //to give homie some time to set the property before we go on

    //DC Current Panel1
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropCurrentPanel1, strlen(cPropCurrentPanel1));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcCurrentPanel1(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                         //to give homie some time to set the property before we go on

    //DC Current Panel2
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropCurrentPanel2, strlen(cPropCurrentPanel2));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcCurrentPanel2(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                         //to give homie some time to set the property before we go on

    //DC Voltage Panel1
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropVoltagePanel1, strlen(cPropVoltagePanel1));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcVoltagePanel1(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                         //to give homie some time to set the property before we go on

    //DC Voltage Panel2
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropVoltagePanel2, strlen(cPropVoltagePanel2));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcVoltagePanel2(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                         //to give homie some time to set the property before we go on

    //DC Power Panel1
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropPowerPanel1, strlen(cPropPowerPanel1));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcPowerPanel1(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                       //to give homie some time to set the property before we go on

    //DC Power Panel2
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropPowerPanel2, strlen(cPropPowerPanel2));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcPowerPanel2(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                       //to give homie some time to set the property before we go on

    //DC Energy Panel1
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropEnergyPanel1, strlen(cPropEnergyPanel1));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcEnergyPanel1(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                        //to give homie some time to set the property before we go on

    //DC Energy Panel2
    memset(&tempReceiveIdBuffer[0], 0, sizeof(tempReceiveIdBuffer)); //zero out all buffers we could work with "messageToDecode"
    delayMicroseconds(250);                                          //give memset a little bit of time to empty all the buffers
    strncpy(tempReceiveIdBuffer, cPropEnergyPanel2, strlen(cPropEnergyPanel2));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    strncat(tempReceiveIdBuffer, tempIndexStr, sizeof(tempIndexStr));
    delayMicroseconds(250); //give memset a little bit of time to empty all the buffers
    Homie.getLogger() << "tempReceiveIdBuffer 1: " << tempReceiveIdBuffer << endl;
    setProperty(tempReceiveIdBuffer).send(String(myYC600.getDcEnergyPanel2(inverterReturnIndex))); //give Feedback to Openhab
    delayMicroseconds(100);                                                                        //to give homie some time to set the property before we go on
}